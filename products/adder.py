import os
import sqlite3


conn = sqlite3.connect('example.db')
c = conn.cursor()
try:
    c.execute('''DROP TABLE product''')
except:
    pass

c.execute('''CREATE TABLE product
        (id, name, img, desc, time)''')

nr=1
for f in os.listdir():
    if f != 'adder.py' and f != 'example.db' and len(f.split('.')) == 1:
        nr+=1
        file_handle = open(f)
        lines = file_handle.readlines()

        id=nr
        name=lines[0].strip()
        img=f
        desc=lines[2].strip()
        time=eval(lines[4])
        print("id: {}".format(id))
        print("name: {}".format(name))
        print("img: {}".format(img))
        print("desc: {}".format(desc))
        print("time: {}".format(time))
        c.execute('INSERT INTO product VALUES (?,?,?,?,?)', [id, name, img, desc, time])


c.execute('''select * from product''')
print (c.fetchall())

conn.commit()

# for f in files:
#     if f != 'adder.py':
#         file_handle = open(f)
        # lines = file_handle.readlines()
        # print(lines)
    
