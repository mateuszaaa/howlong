package com.mateusznowakowski.mat.howlong;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

/**
 * Created by user on 12/8/15.
 */
public class ProductPreviewActivity extends FragmentActivity implements FragmentProductPreview.StartTimerListener {
    private static final String TAG = "PRODUCT_PREVIEW_ACTIVITY";
    FragmentProductPreview preview_fragment;
    FragmentTimer timer_fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_preview);
        preview_fragment = (FragmentProductPreview) getSupportFragmentManager().findFragmentById(R.id.preview_fragment);
        timer_fragment = (FragmentTimer) getSupportFragmentManager().findFragmentById(R.id.timer_fragment);
        getSupportFragmentManager().beginTransaction().hide(timer_fragment).commit();
        if (!timer_fragment.isCountDownAcitve()) {
            preview_product();
            preview_fragment.setListener(this);
        }
    }

    private void preview_product() {
        Bundle b = getIntent().getExtras();
        assert b != null;
        Product p = (Product) b.get(MainActivity.PRODUCT_TAG);
        assert p != null;
        preview_fragment.loadProduct(p);
    }

    @Override
    public void startTimerClicked(Product p) {
        getSupportFragmentManager().beginTransaction().
                hide(preview_fragment).
                show(timer_fragment).
                commit();
        timer_fragment.loadProduct(p);
    }
}
