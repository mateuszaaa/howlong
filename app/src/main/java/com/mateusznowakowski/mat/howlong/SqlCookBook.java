package com.mateusznowakowski.mat.howlong;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SqlCookBook implements CookBook {
    private final String DATABASE_NAME = "db.sqlite";
    private final String TABLE_NAME = "product";
    private final String COL_NAME = "name";
    private final String COL_IMG = "img";
    private final String COL_DESC = "desc";
    private final String COL_TIME = "time";
    private final int COL_NAME_ID = 0;
    private final int COL_IMG_ID = 1;
    private final int COL_DESC_ID = 2;
    private final int COL_TIME_ID = 3;
    private final String[] COLUMNS = new String[] {COL_NAME, COL_IMG, COL_DESC, COL_TIME};
    private final Context ctx;

    public SqlCookBook(Context ctx){
        this.ctx = ctx;
    }

    @Override
    public List<Product> getRecipies() {
        List<Product> list = new ArrayList<>();
        AssetDBOpener opener = new AssetDBOpener(ctx);
        SQLiteDatabase db = opener.openDB(DATABASE_NAME);
        if (db != null) {
            Cursor c = db.query(TABLE_NAME, COLUMNS, null, null, null, null, null);
            for (int i = 0; i < c.getCount(); i++) {
                c.moveToPosition(i);
                String name = c.getString(COL_NAME_ID);
                String img_name = c.getString(COL_IMG_ID);
                String desc = c.getString(COL_DESC_ID);
                int time = c.getInt(COL_TIME_ID);
                list.add(new Product(name, desc, img_name, time));
            }
        }
        Collections.sort(list, new Comparator<Product>() {
            @Override
            public int compare(Product lhs, Product rhs) {
                return lhs.name.compareTo(rhs.name);
            }
        });
        return list;
    }
}
