package com.mateusznowakowski.mat.howlong;

import android.os.Parcel;
import android.os.Parcelable;

public class Product implements Parcelable{
    public int time;
    public String img;
    public String name;
    public String desc;

    Product(String name, String desc, String drawable, int time){
        this.name = name;
        this.desc = desc;
        this.time = time;
        this.img = drawable;
    }

    Product(Parcel in){
        name = in.readString();
        desc = in.readString();
        time = in.readInt();
        img =  in.readString();
    }

    @Override
    public String toString() {
        return String.format("Product{name=%s, img=%s, desc=%s, time=%d }", name, img, desc, time);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(desc);
        dest.writeInt(time);
        dest.writeString(img);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
           public Product createFromParcel(Parcel in) {
               return new Product(in);
           }

           public Product[] newArray(int size) {
               return new Product[size];
           }
       };

    @Override
    public boolean equals(Object o) {
        Product p = (Product) o;
        return (name == p.name && desc == p.desc && img == p.img && time == p.time);
    }
}
