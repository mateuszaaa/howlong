package com.mateusznowakowski.mat.howlong;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

/**
 * Created by user on 12/14/15.
 */
public class NotificationFascade {
    public final static int timer_notif_id = 1;
    public final String notif_title = "Gotowanie...";
    private Context ctx;
    private NotificationManager notif_mgr;
    public NotificationFascade(Context ctx, NotificationManager notif_mgr){
        assert notif_mgr != null;
        assert ctx != null;
        this.notif_mgr = notif_mgr;
        this.ctx = ctx;
    }

    public void showNotification(){
        PendingIntent intent = PendingIntent.getActivity(ctx,
                                                         0,
                                                         new Intent(ctx, DummyActivity.class),
                                                         PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.Builder builder = new Notification.Builder(ctx).
                setContentTitle(notif_title).
                setOngoing(true).
                setSmallIcon(R.mipmap.icon).
                setContentIntent(intent);

//        builder.setContentIntent(intent);
        Notification n = builder.build();
        notif_mgr.notify(timer_notif_id, n);
    }

    public void hideNotification(){
        notif_mgr.cancel(timer_notif_id);
    }

}
