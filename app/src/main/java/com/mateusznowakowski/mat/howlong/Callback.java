package com.mateusznowakowski.mat.howlong;

import java.util.List;

/**
 * Created by mat on 11.08.15.
 */
public interface Callback {
    public void run(List<Product> list);
}
