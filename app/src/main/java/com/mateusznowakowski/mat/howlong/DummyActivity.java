package com.mateusznowakowski.mat.howlong;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by user on 2/22/16.
 */
public class DummyActivity extends Activity{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Log.d("DUMMY", "OnCreate()");
        this.finish();
    }
}
