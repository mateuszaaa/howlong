package com.mateusznowakowski.mat.howlong;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

public class ClockView extends View {

    private int max_value = 1000*30;
    private int ms_left = 0;
    private final int circle_bg = 0xffcfe1f1;
    private final int circle_fg = 0xff40A8EF;
    private final int delta = 40;

    public ClockView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas c) {
        drawCircle(c);
        drawBall(c);
    }

    public void visualiseTimeLeft(int ms){
        if (ms < 0){
            ms = 0;
        }
        if (ms > max_value){
            max_value = ms;
        }
        ms_left = ms;
        postInvalidate();
        invalidate();
    }

    public int getTimeLeft(){
        return ms_left;
    }

    private void drawCircle(Canvas c){
        float scale = (float) (360.0/max_value);

        Rect rectf = new Rect();
        getLocalVisibleRect(rectf);
        Paint paint = new Paint();
        paint.setDither(true);
        paint.setFilterBitmap(true);
        paint.setAntiAlias(true);
        paint.setColor(circle_bg);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(30);


        c.drawArc(rectf.left + delta, rectf.top + delta, rectf.right - delta, rectf.bottom - delta, 0, 360, false, paint);
        paint.setColor(circle_fg);
        c.drawArc(rectf.left + delta, rectf.top + delta, rectf.right - delta, rectf.bottom - delta, -90, ms_left * scale, false, paint);
    }

    private void drawBall(Canvas c) {
        float scale = (float) (360.0/max_value);
        Rect rectf = new Rect();
        getLocalVisibleRect(rectf);
        int center_x = (rectf.right-rectf.left)/2;
        int center_y = (rectf.bottom-rectf.top)/2;
        int r = (rectf.right-rectf.left-2*delta)/2;
        double angle = ms_left*scale;
        int x = (int) (r*Math.sin((angle+180)*Math.PI/180.0));
        int y = (int) (r*Math.cos((angle+180)*Math.PI / 180.0));
        Paint p = new Paint();
        p.setDither(true);
        p.setFilterBitmap(true);
        p.setAntiAlias(true);
        p.setColor(0xff92cfed);
        p.setStrokeWidth(18);
        p.setStyle(Paint.Style.STROKE);
        c.drawCircle(center_x-x,center_y+y, (float) 25.0,p);
        p.setColor(circle_bg);
        c.drawCircle(center_x-x,center_y+y, (float) 7.0,p);
    }
}
