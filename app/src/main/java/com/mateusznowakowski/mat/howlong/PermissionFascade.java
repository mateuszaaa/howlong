package com.mateusznowakowski.mat.howlong;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * Created by user on 1/30/16.
 */
public class PermissionFascade {
    private static final int REQUEST_CODE = 0;
    private static final String[] app_permissions = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.WAKE_LOCK,
            Manifest.permission.DISABLE_KEYGUARD

    };
    private static final String TAG = "PermissionFascade";

    static public void requestPermission(AppCompatActivity act){
        for(String s: app_permissions){
            if (ActivityCompat.checkSelfPermission(act, s) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(act, app_permissions, REQUEST_CODE);
            }else {
                Log.d(TAG, s + " permission granted");
            }
        }
    }
}
