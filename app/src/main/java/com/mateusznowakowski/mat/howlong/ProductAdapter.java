package com.mateusznowakowski.mat.howlong;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;

public class ProductAdapter extends ArrayAdapter<Product> {
    List<Product> products_to_display;
    List<Product> all_products;
    Context ctx;

    public ProductAdapter(Context context, int textViewResourceId, List<Product> products) {
        super(context, textViewResourceId);
        ctx = context;
        products_to_display = products;
        all_products = new ArrayList<>(products);
    }

    @Override
    public Product getItem(int position) {
        return products_to_display.get(position);
    }

    @Override
    public int getCount() {
        return products_to_display.size();
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        Product product;
        if (layoutNotLoaded(v)) {
            v = inflateView(parent);
        }
        product = getItem(position);
        displayProductName(v, product);
        displayCookTime(v, product);
        displayProductImage(v, product);
        return v;
    }

    @Override
    public Filter getFilter() {
        return new ProductFilter(all_products, new Callback(){
            @Override
            public void run(List<Product> list) {
                resetList(list);
            }
        });

    }

    private boolean displayProductImage(View v, Product item) {
        ImageView img = (ImageView) v.findViewById(R.id.image);
        if (!item.img.isEmpty()) {
            Log.d("PRODUCT_ADAPTER", item.name);
            RoundedBitmapDrawable round;
            Resources res = getContext().getResources();
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 10;
            int res_id = getContext().getResources().getIdentifier(item.img, "drawable", getContext().getPackageName());
            Bitmap b = BitmapFactory.decodeResource(res, res_id, options);
            float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, res.getDisplayMetrics());
            int int_px = Math.round(px);
            Bitmap scaled = Bitmap.createScaledBitmap(b, int_px, int_px, false);

            Bitmap bitmap = Bitmap.createBitmap(scaled.getWidth(), scaled.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(bitmap);
            Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
            p.setColor(0xffdddddd);
            p.setStyle(Paint.Style.STROKE);
            p.setStrokeWidth(6);
            c.drawBitmap(scaled, 0, 0, null);
            c.drawCircle(scaled.getWidth() / 2, scaled.getHeight() / 2, scaled.getHeight()/2, p);

            round = RoundedBitmapDrawableFactory.create(getContext().getResources(), bitmap);
            round.setCircular(true);

            img.setImageDrawable(round);
            return true;
        }else {
            Log.w("Product Adapter", "no image id in product structur");
        } return false;
    }

    private void displayProductName(View v, Product item) {
        TextView name = (TextView) v.findViewById(R.id.name);
        name.setText(item.name);
    }

    private void displayCookTime(View v, Product item) {
        TextView cook_time_text_view = (TextView) v.findViewById(R.id.cook_time);
        Formatter text_time = new Formatter();
        int minutes = TimeHelper.getMinutesCount(item.time);
        if (minutes > 0){
            text_time.format("Czas gotowania - %d min", minutes);
            cook_time_text_view.setText(text_time.toString());
        }else{
            cook_time_text_view.setText("Czas gotowania: < 1min");
        }
    }

    private void resetList(List<Product> products){
        this.products_to_display = products;
        notifyDataSetChanged();
    }

    private View inflateView(ViewGroup parent) {
        View v;
        LayoutInflater inflater = ((Activity) ctx).getLayoutInflater();
        v = inflater.inflate(R.layout.product_layout, parent, false);
        return v;
    }

    private boolean layoutNotLoaded(View v) {
        return v == null;
    }

}