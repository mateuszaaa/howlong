    package com.mateusznowakowski.mat.howlong;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


public class FragmentProductPreview extends Fragment {
    private static final String TAG = "FRAGMENT_PRODUCT_PRV";
    private TextView title_tv;
    private ImageView image_iv;
    private TextView description_tv;
    private FloatingActionButton timer_btn;
    private Product product;


    public void setListener(StartTimerListener listener) {
        this.listener = listener;
    }

    public interface StartTimerListener{
        public void startTimerClicked(Product p);
    }

    private StartTimerListener listener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_preview, container, false);
        timer_btn = (FloatingActionButton) v.findViewById(R.id.timer_btn);

        timer_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.startTimerClicked(product);
            }
        });

        return v;
    };

    public void loadProduct(Product p) {
        if (p != null){
            this.product = p;
            update_views_handles();
            setupBackgroundImageAndTitle(p);
            title_tv.setText(p.name);
            description_tv.setText(Html.fromHtml(p.desc));
        }
    }

    private void setupBackgroundImageAndTitle(Product product) {
        int res_id = getProductImageDrawableResourceId(product);
        if (isProductImageAvaible(res_id)) {
            BitmapFactory.Options options = new BitmapFactory.Options();
//            options.inSampleSize = 4;
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), res_id, options);
            setBackgroundImage(bitmap);
        }else{
            Log.e(TAG, "cannot load image");
        }
    }

    private void setBackgroundImage(Bitmap b) {
        Drawable d = new BitmapDrawable(getResources(), b);
        image_iv.setImageDrawable(d);
    }

    private boolean isProductImageAvaible(int res_id) {
        return res_id != 0;
    }

    private int getProductImageDrawableResourceId(Product product) {
        return getContext().getResources().getIdentifier(product.img, "drawable", getContext().getPackageName());
    }

    private void update_views_handles() {
        title_tv = (TextView) getView().findViewById(R.id.preview_name);
        image_iv = (ImageView) getView().findViewById(R.id.preview_img);
        description_tv = (TextView) getView().findViewById(R.id.preview_desc);
        assert title_tv != null;
        assert image_iv != null;
        assert description_tv != null;
    }
}
