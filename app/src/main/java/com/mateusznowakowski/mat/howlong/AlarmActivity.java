package com.mateusznowakowski.mat.howlong;

import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

/**
 * Created by user on 2/2/16.
 */
public class AlarmActivity extends AppCompatActivity {
    private static final String TAG = "ALARM_ACTIVITY";
    MediaPlayer mp;
    Button ok_button;
    View clock_image;
    FlipAnimationController anim_controller;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alarm_layout);
        ok_button = (Button) findViewById(R.id.btn);
        clock_image = findViewById(R.id.image);
        anim_controller = new FlipAnimationController(this, clock_image);

        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        mp = MediaPlayer.create(getApplicationContext(), notification);

        mp.start();
        ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                anim_controller.stop();
                finish();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "OnStart()");
        anim_controller.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        anim_controller.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mp.stop();
    }

}
