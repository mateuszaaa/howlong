package com.mateusznowakowski.mat.howlong;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

/**
 * Created by user on 11/15/15.
 */
public class FlipAnimationController implements Animation.AnimationListener{
    Animation to_middle;
    Animation from_middle;
    private Context ctx;
    private View view;

    FlipAnimationController(Context ctx, View view){
        this.view = view;
        to_middle = AnimationUtils.loadAnimation(ctx, R.anim.to_middle);
        to_middle.setAnimationListener(this);
        from_middle = AnimationUtils.loadAnimation(ctx, R.anim.from_middle);
        from_middle.setAnimationListener(this);
    }

    public void start(){
        view.startAnimation(to_middle);
    }

    public void stop(){
        view.clearAnimation();
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if( animation == from_middle){
            view.startAnimation(to_middle);
        }
        if( animation == to_middle){
            view.startAnimation(from_middle);
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}

