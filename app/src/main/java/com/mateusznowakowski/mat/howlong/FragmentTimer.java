package com.mateusznowakowski.mat.howlong;

import android.app.KeyguardManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

public class FragmentTimer extends Fragment {
    private static final String TAG = "FRAGMENT_TIMER";
    protected Thread thread;
    private AtomicLong target_time = new AtomicLong(0);
    private AtomicBoolean task_active = new AtomicBoolean(false);
    private NotificationFascade notif_fascade;
    private Context ctx;
    //should be injected in loadProduct
    private Product product_handle;
    private Alarm alarm_fascade;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_timer, container, false);
        ctx = getContext();
        Log.d(TAG, "onCreateView()");
        return view;
    }

    public void loadProduct(Product product) {
        product_handle = product;
        final long timestamp = new Date().getTime();
        final long alarm_time = product_handle.time + timestamp;
        if (notif_fascade == null){
            notif_fascade = new NotificationFascade(
                    ctx, (NotificationManager) ctx.getSystemService(ctx.NOTIFICATION_SERVICE));
        }
//        if (alarm_fascade == null){
//            alarm_fascade = new Alarm();
//        }
        target_time.set(alarm_time);
        updateTimeInfo();
        startTimerThread();
    }

    public void setAlarmFascade(Alarm a){
        alarm_fascade = a;
    }

    public void setNotifFascade(NotificationFascade n){
        notif_fascade = n;
    }

    private void startTimerThread() {
        Log.d(TAG, "starting counting down");
        task_active.set(true);
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "thread::run()");
                while (task_active.get()) {
                    try {
                        Thread.sleep(50, 0);
                        updateTimeInfo();
                        if (getTimeLeft() == 0){
                            task_active.set(false);
//                            alarm_fascade.cancelAlarm(getContext());
                            PowerManager pm = (PowerManager) getContext().getSystemService(Context.POWER_SERVICE);
                            PowerManager.WakeLock wakeLock = pm.newWakeLock((PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), "TAG");
                            wakeLock.acquire(10000);

                            KeyguardManager keyguardManager = (KeyguardManager) getContext().getSystemService(Context.KEYGUARD_SERVICE);
                            KeyguardManager.KeyguardLock keyguardLock = keyguardManager.newKeyguardLock("TAG");
                            keyguardLock.disableKeyguard();


                            Intent i = new Intent(ctx, AlarmActivity.class);
                            getContext().startActivity(i);
                            Log.d(TAG, "alarm occured");
                            getActivity().finish();

                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thread.start();
    }


    public void stopTimerTask() {
        Log.d(TAG, "stopping timer task");
        try {
            task_active.set(false);
            if (thread != null) {
                thread.join();
                Log.d(TAG, "thread joined");
            }else{
                Log.d(TAG, "nothing to stop");
            }
            thread = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    private long getTimeLeft() {
        long now = new Date().getTime();
        long ms_time_left = target_time.get() - now;
        return ms_time_left > 0 ? ms_time_left : 0;
    }

    private void updateTimeInfo() {
        Handler h = new Handler(Looper.getMainLooper());
        h.post(new Runnable() {
            @Override
            public void run() {
                View v = getView();
                if (v != null) {
                    ClockView clock = (ClockView) v.findViewById(R.id.clock);
                    clock.visualiseTimeLeft((int) getTimeLeft());
                    displayTimeLeftText((int) getTimeLeft());
                }
            }
        });
    }

    private void displayTimeLeftText(int ms_time_left) {
        View v = getView();
        if (v != null) {
            TextView text_mins = (TextView) v.findViewById(R.id.mins);
            TextView text_sec = (TextView) v.findViewById(R.id.sec);
            text_mins.setText(TimeHelper.getMinutes(ms_time_left));
            text_sec.setText(TimeHelper.getSeconds(ms_time_left));
        }
    }

    public boolean isCountDownAcitve(){
        return task_active.get();
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop()");
        if (getTimeLeft() > 0) {
            notif_fascade.showNotification();
//            alarm_fascade.setAlarm(getContext(), target_time.get());
        }
        super.onStop();
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart()");
        if(product_handle != null) {
            if (notif_fascade != null) {
                notif_fascade.hideNotification();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");
        stopTimerTask();
        if (notif_fascade != null) {
            notif_fascade.hideNotification();
        }
//        if(alarm_fascade != null) {
//            alarm_fascade.release(ctx);
//        }
    }
}

