package com.mateusznowakowski.mat.howlong;

import android.content.Context;


public class CookBookFactory {
    public enum BookType{
        LOCAL,
        SQLITE,
        UNIMPLEMENTED
        }

    public static CookBook get(BookType type, Context ctx) throws ClassNotFoundException {
        switch (type) {
            case LOCAL:
                return new LocalCookBook();
            case SQLITE:
                assert ctx != null;
                return new SqlCookBook(ctx);
            default:
                throw new ClassNotFoundException();
        }
    }

}

