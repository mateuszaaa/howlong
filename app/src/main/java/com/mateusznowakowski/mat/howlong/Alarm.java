package com.mateusznowakowski.mat.howlong;
import android.app.AlarmManager;
import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.PowerManager;
import android.util.Log;

public class Alarm extends BroadcastReceiver
{
    private static final String TAG = "ALARM";
    private static final String ALARM_INTENT_FILTER = "ALARM_HOW_LONG";
    private boolean registered = false;
    PendingIntent pi;

    @Override
    public void onReceive(Context context, Intent intent)
    {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock((PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), "TAG");
        wakeLock.acquire(10000);

        KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock keyguardLock = keyguardManager.newKeyguardLock("TAG");
        keyguardLock.disableKeyguard();
//        // Put here YOUR code.
//        Intent i = new Intent(context, AlarmActivity.class);
//        intent.putExtra("ALARM", true);
//        Log.d(TAG, "alarm occured");
//        cancelAlarm(context);
//        context.startActivity(i);
    }

    public void setAlarm(Context context, long time)
    {
        if (!registered) {
            Log.d(TAG, "broadcast receiver registered");
            context.registerReceiver(this, new IntentFilter(ALARM_INTENT_FILTER));
            registered = true;
        }
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(ALARM_INTENT_FILTER);
        pi = PendingIntent.getBroadcast(context, 0, i, 0);
        am.set(AlarmManager.RTC, time, pi); // Millisec * Second * Minute
        Log.d(TAG, "alarm set");
    }

    public void cancelAlarm(Context context)
    {
        if(pi != null){
            Log.d(TAG, "alarm canceled");
            AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            am.cancel(pi);
            pi = null;
        }else{
            Log.d(TAG, "cannot cancel alarm - not set yet");
        }
    }

    public void release(Context ctx){
        if(registered){
            cancelAlarm(ctx);
            ctx.unregisterReceiver(this);
            registered = false;
            Log.d(TAG, "broadcast receiver unregistered");
        }
    }


}
