package com.mateusznowakowski.mat.howlong;

import android.support.annotation.NonNull;
import android.widget.Filter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mat on 11.08.15.
 */
public class ProductFilter extends Filter {
    private List<Product> products;
    private Callback cbk;
    public ProductFilter(@NonNull List<Product> products, Callback callback){
        this.products = products;
        this.cbk = callback;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        String text = constraint.toString().toLowerCase();
        FilterResults results = new FilterResults();
        List<Product> filtered_products = new ArrayList<>();
        for (Product p : products){
            String lower_case_product_name = p.name.toLowerCase();
            if (text.equals("") || lower_case_product_name.contains(text)){
                filtered_products.add(p);
            }
        }
        results.values = filtered_products;
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        cbk.run((List<Product>) results.values);
    }
}
