package com.mateusznowakowski.mat.howlong;

import java.util.List;

/**
 * Created by mat on 31.07.15.
 */
public interface CookBook{
    public List<Product> getRecipies();
}
