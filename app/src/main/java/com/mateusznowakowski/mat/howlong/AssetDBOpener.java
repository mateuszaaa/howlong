package com.mateusznowakowski.mat.howlong;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by user on 1/20/16.
 */
public class AssetDBOpener {
    private String TAG = "AssetDBOpener";
    private String output_dir;
    private Context ctx;

    public AssetDBOpener(Context ctx){
        output_dir = ctx.getApplicationInfo().dataDir + "/databases";
        this.ctx = ctx;
    }

    public SQLiteDatabase openDB(String db_name){
        SQLiteDatabase db = null;
        try {
            copyDBFromAssetsToInternalMemory(db_name);
            db = SQLiteDatabase.openDatabase(getOutputDBPath(db_name),
                null, SQLiteDatabase.OPEN_READONLY);
        } catch (IOException e) {
            Log.d(TAG, "cannot open database");
        }
        return db;
    }

    private void copyDBFromAssetsToInternalMemory(String db_name) throws IOException {
        byte[] buf = new byte[1024];
        int size;
        InputStream is = ctx.getAssets().open(db_name);
        OutputStream os = new FileOutputStream(getOutputDBPath(db_name));
        while( (size = is.read(buf)) > 0 ){
            os.write(buf, 0 ,size);
        }
    }

    @NonNull
    private String getOutputDBPath(String db_name) {
        return output_dir + db_name;
    }
}
