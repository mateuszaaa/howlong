package com.mateusznowakowski.mat.howlong;

import java.util.Formatter;

/**
 * Created by user on 11/15/15.
 */
public class TimeHelper {
    static public String convertSecondsToString(int _seconds){
        int miliseconds = TimeHelper.secondsToMillisecond(_seconds);
        Formatter text_time = new Formatter();
        int seconds = miliseconds / 1000 % 60;
        int minutes = miliseconds / 1000 / 60;
        text_time.format("%02d:%02d", minutes, seconds);
        return text_time.toString();
    }

    static public String convertMilisecondsToString(int miliseconds){
        Formatter text_time = new Formatter();
        int seconds = miliseconds / 1000 % 60;
        int minutes = miliseconds / 1000 / 60;
        text_time.format("%02d:%02d", minutes, seconds);
        return text_time.toString();
    }

    static public String getMinutes(int miliseconds){
        int minutes = TimeHelper.getMinutesCount(miliseconds);
        Formatter text = new Formatter();
        return text.format("%02d", minutes).toString();
    }

    static public int getMinutesCount(int miliseconds){
        int minutes = miliseconds / 1000 / 60;
        return minutes;
    }

    static public String getSeconds(int miliseconds){
        Formatter text = new Formatter();
        int seconds = miliseconds / 1000 % 60;
        return text.format("%02d", seconds).toString();
    }


    static public int secondsToMillisecond(Integer seconds){
        if (seconds!=null){
            return TimeHelper.secondsToMillisecond(seconds.intValue());
        }else{
            return 0;
        }
    }

    static public int secondsToMillisecond(int seconds){
        if (seconds > 0) {
            return seconds * 1000;
        }else{
            return 0;
        }
    }
}
