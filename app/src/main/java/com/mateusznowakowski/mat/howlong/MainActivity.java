package com.mateusznowakowski.mat.howlong;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;


public class MainActivity extends AppCompatActivity {
    static public final String PRODUCT_TAG = "product";
    ProductAdapter adapter;
    ListView lv;

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lv = (ListView) findViewById(R.id.products_lists);
        setContentView(R.layout.activity_main);
        List<Product> products;

        PermissionFascade.requestPermission(this);

        try {
            CookBook book = CookBookFactory.get(CookBookFactory.BookType.SQLITE, getApplicationContext());
            products = book.getRecipies();
            adapter = new ProductAdapter(this, R.layout.product_layout, products);
        } catch (ClassNotFoundException e) {
        }

        lv = (ListView) findViewById(R.id.products_lists);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long arg3) {
                Product p = (Product) adapter.getItemAtPosition(position);
                Intent i = new Intent(v.getContext(), ProductPreviewActivity.class);
                i.putExtra(PRODUCT_TAG, p);
                startActivity(i);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        SearchView sv = (SearchView) MenuItemCompat.getActionView(item);

        if (sv != null) {
            sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    adapter.getFilter().filter(newText);
                    return false;
                }
            });
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_search) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void loadCookBook(CookBook book) {
        List<Product> products;
        products = book.getRecipies();
        adapter = new ProductAdapter(this, R.layout.product_layout, products);
        lv.setAdapter(adapter);
    }
}
