package com.mateusznowakowski.mat.howlong;

import android.content.Context;
import android.media.MediaPlayer;

/**
 * Created by user on 1/31/16.
 */
public class AlarmSoundPlayer {
    private MediaPlayer media_player;
    public final int alarm_sound = R.raw.alarm;

    public AlarmSoundPlayer(Context ctx) {
        media_player = MediaPlayer.create(ctx, alarm_sound);
    }

    public void start(){
        media_player.reset();
        media_player.start();
    }

    public void stop(){
        media_player.stop();
    }
}
