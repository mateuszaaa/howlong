package com.mateusznowakowski.mat.howlong;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 1/19/16.
 */
public class LocalCookBook implements CookBook {

    @Override
    public List<Product> getRecipies() {
        List<Product> products = new ArrayList<>();
        products.add(new Product("Produkt", "Opis", "corn", 10));
        return products;
    }
}
