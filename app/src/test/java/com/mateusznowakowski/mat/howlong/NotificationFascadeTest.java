package com.mateusznowakowski.mat.howlong;

import android.app.NotificationManager;
import android.content.Context;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;


@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class NotificationFascadeTest extends TestCase {
    NotificationManager notif_mgr;
    @Before
    public void setUp(){
        notif_mgr = (NotificationManager) RuntimeEnvironment.application.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @Test
    public void testShowNotification() throws Exception {
        NotificationFascade notif = new NotificationFascade(RuntimeEnvironment.application, notif_mgr);
        notif.showNotification();
        assertEquals(1, Shadows.shadowOf(notif_mgr).size());
    }

    @Test
    public void testHideNotification() throws Exception {
        NotificationFascade notif = new NotificationFascade(RuntimeEnvironment.application, notif_mgr);
        notif.showNotification();
        notif.hideNotification();
        assertEquals(0, Shadows.shadowOf(notif_mgr).size());
    }

    @Test
    public void testHideNotShowedNotification() throws Exception {
        NotificationFascade notif = new NotificationFascade(RuntimeEnvironment.application, notif_mgr);
        notif.hideNotification();
        assertEquals(0, Shadows.shadowOf(notif_mgr).size());
    }
}