package com.mateusznowakowski.mat.howlong;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.view.View;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import static org.assertj.android.api.Assertions.assertThat;

/**
 * Created by user on 12/9/15.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class ProductPreviewActivityTest extends TestCase {
    ProductPreviewActivity activity;
    @Before
    public void setUp() {
        Context ctx = ShadowApplication.getInstance().getApplicationContext();
        Intent i = new Intent(ctx, ProductPreviewActivity.class);
        Product p = new FakeProduct();
        i.putExtra(MainActivity.PRODUCT_TAG, p);
        activity = Robolectric.buildActivity(ProductPreviewActivity.class).
                withIntent(i).create().visible().start().get();
        assertThat(activity).isNotNull();
    }

    @Test
    public void test_ButtonInitialization(){
        View fragment_preview = activity.findViewById(R.id.preview_fragment);
        FloatingActionButton btn = (FloatingActionButton) fragment_preview.findViewById(R.id.timer_btn);
        assertThat(btn).isNotNull();
        assertThat(btn).isVisible();
    }

    @Test
    public void test_initialFragmentVisibility(){
        View fragment_view = activity.findViewById(R.id.preview_fragment);
        assertThat(fragment_view).isNotNull();
        assertThat(fragment_view).isVisible();
    }

    @Test
    public void test_changeFragmentOnClick(){
        //before
        View fragment_preview = activity.findViewById(R.id.preview_fragment);
        View fragment_timer = activity.findViewById(R.id.timer_fragment);
        FloatingActionButton btn = (FloatingActionButton) fragment_preview.findViewById(R.id.timer_btn);
        assertThat(fragment_preview).isNotNull();
        assertThat(fragment_timer).isNotNull();

        assertThat(fragment_preview).isVisible();
        assertThat(fragment_timer).isNotVisible();

        //action
        btn.performClick();

        //after
        assertThat(fragment_preview).isNotNull();
        assertThat(fragment_preview).isNotVisible();

    }

}