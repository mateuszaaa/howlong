package com.mateusznowakowski.mat.howlong;

import android.os.Parcel;

import junit.framework.TestCase;
import static org.assertj.android.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

/**
 * Created by user on 1/31/16.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class ProductTest extends TestCase {
    private final String test_name = "test_name";
    private final String test_img = "test_img";
    private final String test_desc = "test_desc";
    private final String product_format = "Product{name=%s, img=%s, desc=%s, time=%d }";
    private final int test_time = 10;

    @Test
    public void testToString() throws Exception {
        Product p = new Product(test_name, test_desc, test_img, test_time);
        String expected = String.format(product_format, test_name, test_img, test_desc, test_time);
        assertEquals(expected, p.toString());
    }

    @Test
    public void testDescribeContents() throws Exception {
        Product p = new Product(test_name, test_desc, test_img, test_time);
        assertEquals(p.describeContents(), 0);
    }


    @Test
    public void testWriteToParcel() throws Exception {
        Product original_product = new Product(test_name, test_desc, test_img, test_time);
        Parcel parcel = Parcel.obtain();
        original_product.writeToParcel(parcel, 0);
        parcel.setDataPosition(0);
        Product product_from_parcel = (Product) Product.CREATOR.createFromParcel(parcel);
        assertEquals(product_from_parcel,original_product);
    }
}