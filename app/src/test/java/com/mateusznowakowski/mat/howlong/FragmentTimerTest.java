package com.mateusznowakowski.mat.howlong;

import android.widget.TextView;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class FragmentTimerTest extends TestCase {

    @Test
    public void fragmentNotNull() throws Exception{
        FragmentTimer fragment = new FragmentTimer();
        SupportFragmentTestUtil.startVisibleFragment(fragment);
        assertThat(fragment).isNotNull();

        TextView tv_time_sec = (TextView) fragment.getView().findViewById(R.id.sec);
        TextView tv_time_min = (TextView) fragment.getView().findViewById(R.id.mins);
        ClockView cv_clock = (ClockView) fragment.getView().findViewById(R.id.clock);

        assertThat(tv_time_min).isNotNull();
        assertThat(tv_time_sec).isNotNull();
        assertThat(cv_clock).isNotNull();
    }

    @Test
    public void testLoadUpdateTimeInfo() throws Exception{
        Product product = new FakeProduct();
        FragmentTimer fragment = new FragmentTimer();
        SupportFragmentTestUtil.startVisibleFragment(fragment);
        TextView tv_time_sec = (TextView) fragment.getView().findViewById(R.id.sec);
        TextView tv_time_min = (TextView) fragment.getView().findViewById(R.id.mins);
        ClockView cv_clock = (ClockView) fragment.getView().findViewById(R.id.clock);
        int default_min = 21;
        int default_sec = 30;

        int time_ms = TimeHelper.secondsToMillisecond(product.time);
        assertEquals(fragment.getResources().getString(R.string.f_timer_default_sec),
                    tv_time_sec.getText());
        assertEquals(fragment.getResources().getString(R.string.f_timer_default_min),
                    tv_time_min.getText());

        fragment.loadProduct(product);
        assertNotSame(String.valueOf(default_sec), tv_time_sec.getText()); // timer starts on load product
        assertEquals(TimeHelper.getMinutes(time_ms), tv_time_min.getText());
    }

    @Test
    public void testNotStoppingTimerTaskOnStop() throws Exception{
        Product product = (Product) new FakeProduct();
        product.time = 30*60*1000;
        FragmentTimer fragment = new FragmentTimer();
        SupportFragmentTestUtil.startVisibleFragment(fragment);
        assertFalse(fragment.isCountDownAcitve());
        fragment.loadProduct(product);
        assertTrue(fragment.isCountDownAcitve());
        fragment.onStop();
        assertTrue(fragment.isCountDownAcitve());
    }

    @Test
    public void testStoppingTimerTaskOnDestroy() throws Exception{
        Product product = (Product) new FakeProduct();
        product.time = 30;
        FragmentTimer fragment = new FragmentTimer();
        SupportFragmentTestUtil.startVisibleFragment(fragment);
        assertFalse(fragment.isCountDownAcitve());
        fragment.loadProduct(product);
        assertTrue(fragment.isCountDownAcitve());
        fragment.onDestroy();
        assertFalse(fragment.isCountDownAcitve());
    }

//    @Test
//    public void testNoAlarmAndNoNotificationInOnStartWhenAlarmNotSet() throws Exception{
//        ShadowLog.stream = System.out;
//        Product product = (Product) new FakeProduct();
//        product.time = 30*60*1000;
//        FragmentTimer fragment = new FragmentTimer();
//        Alarm alarm_mock = mock(Alarm.class);
//        NotificationFascade notif_mock = mock(NotificationFascade.class);
//        fragment.setAlarmFascade(alarm_mock);
//        fragment.setNotifFascade(notif_mock);
//        fragment.loadProduct(product);
//        SupportFragmentTestUtil.startFragment(fragment);
//        verify(alarm_mock, times(1)).cancelAlarm(any(Context.class));
//        verify(notif_mock, times(1)).hideNotification();
//    }
//
//    @Test
//    public void testSetAlarmAndNotificationInOnStopWhenAlarmSet() throws Exception{
//        ShadowLog.stream = System.out;
//        Product product = (Product) new FakeProduct();
//        product.time = 30*60*1000;
//        FragmentTimer fragment = new FragmentTimer();
//        Alarm alarm_mock = mock(Alarm.class);
//        NotificationFascade notif_mock = mock(NotificationFascade.class);
//        fragment.setAlarmFascade(alarm_mock);
//        fragment.setNotifFascade(notif_mock);
//        fragment.loadProduct(product);
//        SupportFragmentTestUtil.startFragment(fragment);
//        fragment.onStop();
//        verify(alarm_mock, times(1)).setAlarm(any(Context.class), any(long.class));
//        verify(notif_mock, times(1)).showNotification();
//        fragment.onStart();
//        verify(alarm_mock, times(2)).cancelAlarm(any(Context.class));
//        verify(notif_mock, times(2)).hideNotification();
//    }
}