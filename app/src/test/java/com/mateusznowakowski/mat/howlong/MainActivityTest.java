package com.mateusznowakowski.mat.howlong;

import android.content.Intent;
import android.view.View;
import android.widget.Adapter;
import android.widget.ListView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class MainActivityTest {

    private MainActivity activity;

    public ListView getProductListView(){
        ListView products_list = (ListView) activity.findViewById(R.id.products_lists);
        assertNotNull(products_list);
        return products_list;
    }

    @Before
    public void setup() {
        activity = Robolectric.buildActivity(MainActivity.class).create().start().visible().get();
        assertNotNull(activity);
    }

    @Test
    public void reloadCookBook() {
        int recipies_count;
        CookBook book;
        ListView products_list = getProductListView();


        recipies_count = 0;
        book = new FakeCookBook(recipies_count);
        activity.loadCookBook(book);
        assertEquals(products_list.getCount(), recipies_count);

        recipies_count = 10;
        book = new FakeCookBook(recipies_count);
        activity.loadCookBook(book);
        assertEquals(products_list.getCount(), recipies_count);


    }

    @Test
    public void clickProduct() {
        final int item_position = 0;
        final int recipies_count = 1;
        ListView products_list;

        CookBook book = new FakeCookBook(recipies_count);

        activity = Robolectric.buildActivity(MainActivity.class).create().start().visible().get();
        products_list = getProductListView();
        assertNotNull(products_list);
        activity.loadCookBook(book);

        Adapter list_adapter = products_list.getAdapter();
        View item_view = list_adapter.getView(item_position, null, products_list);

        products_list.performItemClick(item_view, item_position, list_adapter.getItemId(item_position));

        ShadowActivity shadow = Shadows.shadowOf(activity);
        Intent opened_intent = shadow.getNextStartedActivity();
        Intent expected_intent = new Intent(activity, ProductPreviewActivity.class);
        expected_intent.putExtra(MainActivity.PRODUCT_TAG, book.getRecipies().get(0));

        assertEquals(expected_intent, opened_intent);
    }

}