package com.mateusznowakowski.mat.howlong;

import android.widget.ImageView;
import android.widget.TextView;

import junit.framework.TestCase;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class FragmentProductPreviewTest extends TestCase {

    @Test
    public void fragmentNotNull() throws Exception{
        FragmentProductPreview fragment = new FragmentProductPreview();
        SupportFragmentTestUtil.startVisibleFragment(fragment);
        assertThat(fragment).isNotNull();

    }

    @Test
    public void initializeWithData() throws Exception{
        TextView title_view;
        ImageView img_view;
        TextView desc_view;

        Product product = new FakeProduct();
        FragmentProductPreview fragment = new FragmentProductPreview();
        SupportFragmentTestUtil.startVisibleFragment(fragment);
        title_view = (TextView) fragment.getView().findViewById(R.id.preview_name);
        img_view = (ImageView) fragment.getView().findViewById(R.id.preview_img);
        desc_view = (TextView) fragment.getView().findViewById(R.id.preview_desc);

        //before
        assertThat(title_view.getText()).isNotEqualTo(product.name);
        assertThat(desc_view.getText()).isNotEqualTo(product.desc);

        //action
        fragment.loadProduct(product);

        //after
        assertThat(title_view.getText()).isEqualTo(product.name);
        assertThat(desc_view.getText().toString()).isEqualTo(product.desc);
    }


}