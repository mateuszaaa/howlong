package com.mateusznowakowski.mat.howlong;

import android.content.Context;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by user on 1/31/16.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class CookBookFactoryTest extends TestCase {
    private Context ctx;
    @Before
    public void SetUp(){
        ctx = ShadowApplication.getInstance().getApplicationContext();
        assertThat(ctx).isNotNull();
    }

    @Test
    public void testGetLocalCookBook() throws Exception {
        CookBook c = CookBookFactory.get(CookBookFactory.BookType.LOCAL,ctx);
        assertThat(c).isNotNull();
        List<Product> recipies = c.getRecipies();
        assertThat(recipies).isNotNull();
        assertTrue(recipies.size() > 0);
    }

    @Test
    public void testGetSqlCookBook() throws Exception {
        CookBook c = CookBookFactory.get(CookBookFactory.BookType.SQLITE,ctx);
        assertThat(c).isNotNull();
        List<Product> recipies = c.getRecipies();
        assertThat(recipies).isNotNull();
        assertTrue(recipies.size() > 0);
    }

    @Test
    public void testGetCookBookThatDoesntExists() throws Exception {
        Throwable e = null;
        try {
            CookBook c = CookBookFactory.get(CookBookFactory.BookType.UNIMPLEMENTED, ctx);
        }catch (ClassNotFoundException exc){
            e = exc;
        }
        assertTrue(e instanceof ClassNotFoundException);
    }
}