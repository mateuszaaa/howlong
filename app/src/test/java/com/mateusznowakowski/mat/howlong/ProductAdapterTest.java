package com.mateusznowakowski.mat.howlong;

import android.content.Context;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.android.api.Assertions.assertThat;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class ProductAdapterTest extends TestCase {
    private Context ctx;
    @Before
    public void SetUp(){
        ctx = ShadowApplication.getInstance().getApplicationContext();
    }

    @Test
    public void testGetItem() throws Exception {
        List<Product> products_list = new ArrayList<>();
        for (int i = 0 ; i < 5; ++i){
           products_list.add(new FakeProduct());
        }
        ProductAdapter adapter = new ProductAdapter(ctx, R.layout.product_layout, products_list);
        assertThat(adapter).isNotNull();
        Product product_from_adapter = adapter.getItem(0);
        Product product_from_list = (Product) products_list.get(0);
        Product wron_product_from_list = (Product) products_list.get(1);
        assertSame(product_from_adapter, product_from_list);
        assertNotSame(product_from_adapter, wron_product_from_list);

    }

    @Test
    public void testGetCount() throws Exception {
        List<Product> products_list = new ArrayList<>();
        for (int i = 0 ; i < 5; ++i){
            products_list.add(new FakeProduct());
        }
        ProductAdapter adapter = new ProductAdapter(ctx, R.layout.product_layout, products_list);
        assertEquals(products_list.size(), adapter.getCount());
    }

    @Test
    public void testGetFilter() throws Exception {
        List<Product> products_list = new ArrayList<>();
        for (int i = 0 ; i < 5; ++i){
            products_list.add(new FakeProduct());
        }
        ProductAdapter adapter = new ProductAdapter(ctx, R.layout.product_layout, products_list);
        assertNotNull(adapter.getFilter());
    }
}