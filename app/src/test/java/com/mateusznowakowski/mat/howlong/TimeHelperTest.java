package com.mateusznowakowski.mat.howlong;

import junit.framework.TestCase;

import org.junit.Test;


public class TimeHelperTest extends TestCase {

    @Test
    public void testConvertToStringOK() throws Exception {
        String time;

        time = TimeHelper.convertMilisecondsToString(1000);
        assertEquals("00:01", time);

        time = TimeHelper.convertMilisecondsToString(2000);
        assertEquals("00:02", time);

        time = TimeHelper.convertMilisecondsToString(60 * 1000);
        assertEquals("01:00", time);

        time = TimeHelper.convertMilisecondsToString(60 * 1500);
        assertEquals("01:30", time);

        time = TimeHelper.convertMilisecondsToString(0);
        assertEquals("00:00", time);

        time = TimeHelper.convertMilisecondsToString(-1);
        assertEquals("00:00", time);
    }

    @Test
    public void testconvertSecondsToString(){
        String time;
        time = TimeHelper.convertSecondsToString(0);
        assertEquals("00:00", time);

        time = TimeHelper.convertSecondsToString(-1);
        assertEquals("00:00", time);

        time = TimeHelper.convertSecondsToString(30);
        assertEquals("00:30", time);

        time = TimeHelper.convertSecondsToString(60);
        assertEquals("01:00", time);

        time = TimeHelper.convertSecondsToString(90);
        assertEquals("01:30", time);
    }

    }