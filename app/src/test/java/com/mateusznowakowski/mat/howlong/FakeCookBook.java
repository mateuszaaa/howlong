package com.mateusznowakowski.mat.howlong;

import java.util.ArrayList;
import java.util.List;

public class FakeCookBook implements CookBook {
    List <Product> list;
    public FakeCookBook(int count){
        list = new ArrayList<>();
        int i;
        for(i=0 ; i<count; ++i){
            list.add(new FakeProduct());
        }
    }
    @Override
    public List<Product> getRecipies() {
        return list;
    }
}
